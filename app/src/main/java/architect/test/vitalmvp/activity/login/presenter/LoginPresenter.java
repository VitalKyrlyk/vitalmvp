package architect.test.vitalmvp.activity.login.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import architect.test.vitalmvp.activity.login.LoginActivity;
import architect.test.vitalmvp.activity.login.model.LoginItemModel;
import architect.test.vitalmvp.activity.register.model.RegisterItemModel;
import architect.test.vitalmvp.activity.register.presenter.realm.PersonRealmHelper;
import ru.arturvasilov.rxloader.LifecycleHandler;

public class LoginPresenter {

    private final LifecycleHandler lifecycleHandler;
    private final LoginActivity loginActivity;
    private List<RegisterItemModel> registerItemModels;
    private LoginItemModel loginItemModel;


    public LoginPresenter(@NonNull LifecycleHandler lifecycleHandler,
                          @NonNull LoginActivity loginActivity, Context applicationContext) {
        this.lifecycleHandler = lifecycleHandler;
        this.loginActivity = loginActivity;
        PersonRealmHelper personRealmHelper = new PersonRealmHelper(applicationContext);
        registerItemModels = personRealmHelper.show();
        loginItemModel = new LoginItemModel(applicationContext.getSharedPreferences("User", Context.MODE_PRIVATE));
    }

    public void init() {

    }

    public void tryLogIn(@NonNull String login, @NonNull String password) {

        for (RegisterItemModel c : registerItemModels) {

            if (login.equals(c.getLogin()) && password.equals(c.getPass())) {
                Log.d("myTAG", "Login ok " + c.getLogin());
                loginItemModel.saveUser(c.getLogin(), c.getPass(), c.getName(),
                        c.getSecName(), c.getDate(), c.getMail(), c.getId());
                loginActivity.openMainScreen();
                break;
            } else {
                loginActivity.showLoginError();
//                break;
            }
        }
    }
}
