package architect.test.vitalmvp.activity.register.presenter.realm;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;

import architect.test.vitalmvp.activity.register.model.RegisterItemModel;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class PersonRealmHelper {

    private Realm realm;
    private RealmResults<PersonObject> realmResults;
    private Context context;

    public PersonRealmHelper(Context context) {

        Realm.init(context);
        this.context = context;
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(config);

    }

    private int getCount() {
        realmResults = realm.where(PersonObject.class).findAll();
        return realmResults.size();
    }

    public void add(@NonNull String login, @NonNull String password, @NonNull String name,
                    @NonNull String secName, @NonNull String date, @NonNull String mail) {
        PersonObject personObject = new PersonObject();
        personObject.setId(getCount() + 1);
        personObject.setLogin(login);
        personObject.setPass(password);
        personObject.setName(name);
        personObject.setSecName(secName);
        personObject.setDate(date);
        personObject.setMail(mail);

        realm.beginTransaction();
//        realm.insert(personObject);
        realm.copyToRealm(personObject);
        realm.commitTransaction();

        Log.d("myTAG", "Helper: size: " + getCount());
        Log.d("myTAG", "Helper: add person: " + personObject);
    }

    public ArrayList<RegisterItemModel> show() {
        long id;
        String login, password, name, secName, date, mail;
        ArrayList<RegisterItemModel> data = new ArrayList<>();

        realmResults = realm.where(PersonObject.class).findAll();
        realmResults.sort("Id");

        if (realmResults.size() > 0) {
            for (int i = 0; i < realmResults.size(); i++) {
                id = realmResults.get(i).getId();
                name = realmResults.get(i).getName();
                login = realmResults.get(i).getLogin();
                password = realmResults.get(i).getPass();
                secName = realmResults.get(i).getSecName();
                date = realmResults.get(i).getDate();
                mail = realmResults.get(i).getMail();
                data.add(new RegisterItemModel(id, name, login, password, secName, date, mail));
            }
        }

        return data;
    }

}
