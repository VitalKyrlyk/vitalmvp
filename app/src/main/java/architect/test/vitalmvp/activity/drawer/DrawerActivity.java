package architect.test.vitalmvp.activity.drawer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import architect.test.vitalmvp.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DrawerActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    @BindView(R.id.textView2)
    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences("User", MODE_PRIVATE);
        String login = sharedPreferences.getString("Login", "");
        textView.setText(login);

    }
}
