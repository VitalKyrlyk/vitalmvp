package architect.test.vitalmvp.activity.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import architect.test.vitalmvp.R;
import architect.test.vitalmvp.activity.login.LoginActivity;
import architect.test.vitalmvp.activity.register.presenter.RegisterPresenter;
import architect.test.vitalmvp.activity.register.view.RegisterView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

public class RegisterActivity extends AppCompatActivity implements RegisterView {

    RegisterPresenter registerPresenter;

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etSecName)
    EditText etSecName;

    @BindView(R.id.etDate)
    EditText etDate;

    @BindView(R.id.etLogin)
    EditText etLogin;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPass)
    EditText etPass;

    @BindView(R.id.etPassConfirm)
    EditText etPassConfirm;

    @OnClick((R.id.btnRegisterRegistActivity))
    public void btnRegisterRegistActivity(){
        String confirmPass = etPassConfirm.getText().toString();
        String password = etPass.getText().toString();
        String mail = etEmail.getText().toString();
        String login = etLogin.getText().toString();
        String date = etDate.getText().toString();
        String secName = etSecName.getText().toString();
        String name = etName.getText().toString();

        registerPresenter.tryRegister(login, password, name,
                secName, date, mail, confirmPass);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        ButterKnife.bind(this);
        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(this, getSupportLoaderManager());
        registerPresenter = new RegisterPresenter(lifecycleHandler, this, getApplicationContext());

    }

    @Override
    public void openLoginScreen() {

        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    @Override
    public void showCopyLoginError() {

        etLogin.setError("This login already used");

    }

    @Override
    public void showLoginError() {

        etLogin.setError("This field must be 4-12 symbol");

    }

    @Override
    public void showPasswordError() {

        etPass.setError("This field must be 8-16 symbol");

    }

    @Override
    public void showSecNameError() {

        etSecName.setError("This field must not be empty");

    }

    @Override
    public void showNameError() {

        etName.setError("This field must not be empty");

    }

    @Override
    public void showDateError() {

        etDate.setError("This field must not be empty");

    }

    @Override
    public void showMailError() {

        etEmail.setError("This field must not be empty");

    }

    @Override
    public void showConfirmPassError() {

        etPassConfirm.setError("Passwords do not match");

    }

}
