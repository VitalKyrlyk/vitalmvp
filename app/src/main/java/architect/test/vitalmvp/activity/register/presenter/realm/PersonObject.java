package architect.test.vitalmvp.activity.register.presenter.realm;

import io.realm.RealmObject;

public class PersonObject extends RealmObject {

    private String name;
    private String SecName;
    private String Login;
    private String Mail;
    private String Pass;
    private String Date;
//    private String Image;
    private long Id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecName() {
        return SecName;
    }

    public void setSecName(String SecName) {
        this.SecName = SecName;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String Mail) {
        this.Mail = Mail;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

//    public String getImage() {
//        return Image;
//    }
//
//    public void setImage(String Image) {
//        this.Image = Image;
//    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }
}
