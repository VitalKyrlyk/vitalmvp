package architect.test.vitalmvp.activity.register.model;

public class RegisterItemModel {

    private String Name;
    private String SecName;
    private String Login;
    private String Mail;
    private String Pass;
    private String Date;
    //    private String Image;
    private long Id;

    public RegisterItemModel(long id, String name, String login, String password,
                              String secName, String date, String mail) {
        this.Name = name;
        this.Login = login;
        this.Pass = password;
        this.SecName = secName;
        this.Date = date;
        this.Mail = mail;
        this.Id = id;

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getSecName() {
        return SecName;
    }

    public void setSecName(String SecName) {
        this.SecName = SecName;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String Mail) {
        this.Mail = Mail;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

//    public String getImage() {
//        return Image;
//    }
//
//    public void setImage(String Image) {
//        this.Image = Image;
//    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

}
