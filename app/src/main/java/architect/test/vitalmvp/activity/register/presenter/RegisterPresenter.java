package architect.test.vitalmvp.activity.register.presenter;

import android.content.Context;
import android.util.Log;

import java.util.List;

import architect.test.vitalmvp.activity.register.RegisterActivity;
import architect.test.vitalmvp.activity.register.model.RegisterItemModel;
import architect.test.vitalmvp.activity.register.presenter.realm.PersonRealmHelper;
import ru.arturvasilov.rxloader.LifecycleHandler;

public class RegisterPresenter {

    private final LifecycleHandler lifecycleHandler;
    private final RegisterActivity registerActivity;
    private PersonRealmHelper personRealmHelper;
    private List<RegisterItemModel> registerItemModel;


    public RegisterPresenter(LifecycleHandler lifecycleHandler, RegisterActivity registerActivity, Context applicationContext) {
        this.lifecycleHandler = lifecycleHandler;
        this.registerActivity = registerActivity;
        personRealmHelper = new PersonRealmHelper(applicationContext);
        registerItemModel = personRealmHelper.show();
    }

    public void tryRegister(String login, String password, String name,
                             String secName, String date, String mail,
                             String confirmPass){
        String loginIs = null;

        if (registerItemModel.size() > 0) {
            for (int i = 0; i < registerItemModel.size(); i++) {
                loginIs = registerItemModel.get(i).getLogin();
            }
        }

        if (name.isEmpty()){
            registerActivity.showNameError();
        } else if (secName.isEmpty()){
            registerActivity.showSecNameError();
        } else if (date.isEmpty()){
            registerActivity.showDateError();
        } else if ((login.isEmpty()) || (login.length() < 4) ||
                (login.length() > 12)){
            registerActivity.showLoginError();
        } else if (login.equals(loginIs)){
             registerActivity.showCopyLoginError();
        } else if (mail.isEmpty()){
            registerActivity.showMailError();
        } else if ((password.isEmpty()) || (password.length() < 8) ||
                (password.length() > 16)  ) {
            registerActivity.showPasswordError();
        } else if (!confirmPass.equals(password)){
            registerActivity.showConfirmPassError();
        } else {
            personRealmHelper.add(login, password, name, secName, date, mail);
            registerActivity.openLoginScreen();
            Log.d("myTAG", "Presenter: addOK, login:" + login);
        }

    }

}
