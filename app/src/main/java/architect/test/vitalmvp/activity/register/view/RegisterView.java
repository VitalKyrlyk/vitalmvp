package architect.test.vitalmvp.activity.register.view;

public interface RegisterView {

    void openLoginScreen();

    void showLoginError();

    void showCopyLoginError();

    void showPasswordError();

    void showSecNameError();

    void showNameError();

    void showDateError();

    void showMailError();

    void showConfirmPassError();


}
