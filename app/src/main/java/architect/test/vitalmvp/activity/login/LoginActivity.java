package architect.test.vitalmvp.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import architect.test.vitalmvp.R;
import architect.test.vitalmvp.activity.drawer.DrawerActivity;
import architect.test.vitalmvp.activity.login.presenter.LoginPresenter;
import architect.test.vitalmvp.activity.login.view.LoginView;
import architect.test.vitalmvp.activity.register.RegisterActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

public class LoginActivity extends AppCompatActivity implements LoginView {

    LoginPresenter loginPresenter;

    @BindView(R.id.etLoginLogAct)
    EditText etLoginLogAct;

    @BindView(R.id.etPassLogAct)
    EditText etPassLogAct;

    @OnClick(R.id.btnLogin)
    public void onLogInBtnClick(){

        String login = etLoginLogAct.getText().toString();
        String password = etPassLogAct.getText().toString();

        loginPresenter.tryLogIn(login, password);

    }

    @OnClick(R.id.btnRegister)
    public void  registerBtnClick(){

        openRegistrationScreen();
        Log.d("myTAG", "BTN register ");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(this, getSupportLoaderManager());
        loginPresenter = new LoginPresenter(lifecycleHandler, this, getApplicationContext());
        loginPresenter.init();
    }

    @Override
    public void openMainScreen() {

        Intent intent = new Intent(this, DrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        Log.d("myTAG", "Open Main Screen");

    }

    @Override
    public void showLoginError() {

        Toast.makeText(this, "Not found account with this login or password is not correctly!",
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void openRegistrationScreen() {

        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);

        Log.d("myTAG", "Open registration screen");

    }

}
