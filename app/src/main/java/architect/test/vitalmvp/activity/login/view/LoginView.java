package architect.test.vitalmvp.activity.login.view;

public interface LoginView {

    void openMainScreen();

    void showLoginError();

    void openRegistrationScreen();

}
