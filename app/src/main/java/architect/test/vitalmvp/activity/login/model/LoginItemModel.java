package architect.test.vitalmvp.activity.login.model;

import android.content.SharedPreferences;

public class LoginItemModel {

    private final String SAVED_Login = "Login";
    private final String SAVED_Name = "Name";
    private final String SAVED_SecName = "SecName";
    private final String SAVED_Mail = "Mail";
    private  final String SAVED_Pass = "Pass";
    private  final String SAVED_Date = "Date";
//    final String SAVED_Image = "Image";
    private  final String SAVED_Id = "Id";
    private SharedPreferences sharedPreferences;

    public LoginItemModel(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void saveUser(String login, String password, String name,
                         String secName, String date, String mail, long id) {

                SharedPreferences.Editor ed = sharedPreferences.edit();

                ed.putString(SAVED_Login, login);
                ed.putString(SAVED_Pass, password);
                ed.putString(SAVED_Mail, mail);
                ed.putString(SAVED_Name, name);
                ed.putString(SAVED_SecName, secName);
                ed.putString(SAVED_Date, date);
//                ed.putString(SAVED_Image, personObject.getImage());
                ed.putLong(SAVED_Id, id);
                ed.apply();

            }
}